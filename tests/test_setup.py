""" Same checks to see if new data is consistent with the previous upload
"""

import urllib.request
from pathlib import Path
import tempfile
import os
import re
import sys

from cookiecutter.main import cookiecutter

PREVIOUS_ENV_FILE = "https://gitlab.com/exiobase/satellites/extension_template/-/raw/main/%7B%7B%20cookiecutter.project_slug%20%7D%7D/environment.yml?inline=false"

TESTPATH = Path(os.path.dirname(os.path.abspath(__file__)))

# TESTPATH = Path('/home/konstans/proj/exio_satellite/sat_template/tests/')
PACKAGEPATH = TESTPATH.parent
COOKIEPATH = PACKAGEPATH / "{{ cookiecutter.project_slug }}"


def test_compare_env_files():
    """Tests if all the packages previously in the environment are still there

    This does not test consistent package versions as this might change regularly.
    It guards against accidental removal of packages.
    """

    def get_env_content(env_file):
        package_pattern = re.compile(r"[a-zA-Z\-]")
        all_lines = env_file.read_text().splitlines()
        relevant_lines = [
            "".join(package_pattern.findall(ll.strip()[1:].strip()))
            for ll in all_lines
            if len(ll) > 1 and ll.strip()[0] == "-"
        ]
        return set(relevant_lines)

    with tempfile.TemporaryDirectory() as tmpdir:

        old_env_file = Path(tmpdir) / "environment_old.yml"

        new_env_file = Path(COOKIEPATH / "environment.yml")

        urllib.request.urlretrieve(PREVIOUS_ENV_FILE, old_env_file)

        old_lines = get_env_content(old_env_file)
        new_lines = get_env_content(new_env_file)

        assert old_lines.issubset(
            new_lines
        ), "Packages or channels have been removed from the environment file"


def test_cookiecutter():
    """Testing cookiecutter setup and some conf functionality"""
    with tempfile.TemporaryDirectory() as tmpdir:
        os.chdir(tmpdir)
        new_repo = cookiecutter(str(PACKAGEPATH), no_input=True)
        os.chdir(Path(new_repo) / "src")
        # Import in pytest does not work unless the path is in PATH, so:
        sys.path.insert(0, os.getcwd())
        import conf

        req_folders_root = [f.name for f in COOKIEPATH.iterdir() if f.is_dir()]
        for ff in req_folders_root:
            if ff[0] in [".", "_"]:
                continue
            assert ff in dir(conf.proj)

        req_folders_data = [f.name for f in conf.proj.data.iterdir() if f.is_dir()]

        for ff in req_folders_data:
            assert ff in dir(conf.data)
