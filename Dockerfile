FROM condaforge/mambaforge:4.11.0-0 

LABEL author="Konstantin Stadler <konstantin.stadler@ntnu.no>"
LABEL description="Container for testing the satellite account template, can be used for development as well"

# # User / workdir settings 
ARG USERNAME=exionado
ENV EXIODIR=/exio
ENV USER=$USERNAME

RUN mkdir ${EXIODIR}
WORKDIR ${EXIODIR}

RUN useradd -ms /bin/bash "$USER" 

# Setup environment and clean it up
ARG env_src="{{ cookiecutter.project_slug }}/environment.yml"
COPY ${env_src} .

RUN mamba env create --force --file environment.yml && \
    conda clean -tipsy && \
    find ${CONDA_DIR} -follow -type f -name '*.a' -delete && \
    find ${CONDA_DIR} -follow -type f -name '*.pyc' -delete && \
    conda clean -afy && \
    chmod -R a+rwx "${CONDA_DIR}" "/home" "/etc/" && \
    echo ". ${CONDA_DIR}/etc/profile.d/conda.sh && conda activate exio_sat_pipeline" >> /etc/skel/.bashrc && \
    echo ". ${CONDA_DIR}/etc/profile.d/conda.sh && conda activate exio_sat_pipeline" >> ~/.bashrc && \
    echo ". ${CONDA_DIR}/etc/profile.d/conda.sh && conda activate exio_sat_pipeline" >> /home/${USER}/.bashrc

# Add tini
ENV TINI_VERSION v0.19.0
ADD https://github.com/krallin/tini/releases/download/${TINI_VERSION}/tini /tini
RUN chmod +x /tini

USER ${USER}

ENTRYPOINT ["/tini", "--"]
CMD [ "/bin/bash" ] 

# DEVELOPMENT NOTES
#
# The image currently is about 1.4 GB. 
# Attempts to reduce the size which failed: 
# - multistage build (copying the CONDA_DIR to an empty slim container): reduced size by around 200MB but 4x build time
# - alpine container: failed due to some dependencies. Alpine is very far from the usual production environment and should not be used for python
