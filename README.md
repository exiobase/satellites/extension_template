# Template for EXIOBASE extensions

This repository provides the template structure for a new EXIOBASE extension (satellite accounts) pipeline.

Each satellite extension should be build based on this structure, one for each common data source.

## Installation

You need the [cookiecutter package](https://github.com/cookiecutter/cookiecutter) to start with this project.

You can install cookiecutter either with conda

```bash
conda install -c conda-forge cookiecutter
```

or pip

```bash
pip install cookiecutter
```

Then you can make a new extension template with 

```bash
cookiecutter gl:exiobase/satellites/extension_template
```

This will ask you a couple of questions regarding the new satellite account and then generate an initial folder structure, a README.md and some first code files to get you started.

The generated README.md explains the next steps you should do after generating a new project.

## Advanced usage

We also provide a docker container with the environment already installed. 

You can find these in the [container
registry](https://gitlab.com/exiobase/satellites/extension_template/container_registry/2739679 ).
For most use cases we recommend to use the stable container which is build anew
for each new release (git tag hashes). The latest tag is used for testing 
and is only rebuild if the Dockerfile or the environment.yml changes.


## Development

The template source is at 

./{{ cookiecutter.project_slug }} 

There are several cookiecutter variables available, see the ./{{ cookiecutter.project_slug }}/README.md 
for an example and further check the cookiecutter docs.

The questions we ask when establishing a new extension project are defined in
the cookiecutter.json .

Report all changes in the CHANGELOG.md file! We use data based version
numbering/tagging - e.g. v22.02.04 for a release in 2022 Feb 04. This will help
to reconstruct previous runs.

### Structure

The structure is defined in the subdirectories of ./{{ cookiecutter.project_slug }}

Note that empty directories are not tracked by git. To have them in the structure we add an empty .gitkeep file.

### Code style

Please use the [black code formater](https://github.com/psf/black) for
formating - it is included in the environment (make sure you use the same
version if you install manually - code format is checked in the tests).

### Testing

We use pytest. 
Tests run in a container build with the specs in the Dockerfile - check ./.gitlab-ci.yml for furter details.

