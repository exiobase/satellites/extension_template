# EXIOBASE Extensions Handling Tutorial 

Here we use the new matrial data (new by Spring 2022) to give a full walk-through on how to setup a new extension for EXIOBASE.

The material data-set comes already nicely formatted without any major formatting or data issues, thus an easy entry into the functionality.

Note: the tutorial was compiled on Linux, but the whole system also works on Windows (fingers crossed).


## Software requirements

You need Python and [Conda](https://docs.conda.io/projects/conda/en/latest/user-guide/getting-started.html) to follow the tutorial.

Principally, there is no problem running the extension data with another Python environment system (virtual environment or similar), but you would need to define the environment yourself.

So, first we make sure that we have conda installed (open your shell and run)

~~~ bash
conda
~~~

If this works, we are good to go. 

If not, you need to install [Conda](https://docs.conda.io/projects/conda/en/latest/user-guide/install/index.html) or [Anaconda](https://www.anaconda.com/products/distribution) (which is Conda + pre-installed Python data science libraries).

Optional: to speed up everything you can install [Mamba](https://github.com/mamba-org/mamba), which is a faster package manager for installing conda packages.

~~~ bash
conda install mamba
~~~

Once we have this in place the last requirement is [Cookiecutter](https://github.com/cookiecutter/cookiecutter), which allows us to automatically setup a pre-defined project structure (replace conda with mamba if you have installed that before):

~~~ bash
conda install -c conda-forge cookiecutter
~~~

## Location

We need a place to store the extensions.
For the initial runs, that can be anywhere (locally or whatever), but later on it needs to be on a location which can be automatically assessed by the main EXIOBASE. 

So if you follow along, start with a local directory. Later move the directory (or start-over, everything should be reproducible and automatically runable) at TODO FILL IN EXIOBASE EXTENSION DIR.

Do not make the directory where the specific extension should be stored (this will be done automatically).
Instead, move to some kind of root directory where you want to store all your extension data (e.g. /data/extensions or similar).


## Bootstrapping a new extension project 

With everything in place and being at our project/data root directory we run

~~~ bash
cookiecutter gl:exiobase/satellites/extension_template
~~~

This will ask you a couple of questions

1. Full name of the extension. Here we use "Material Data WU"
2. Project slug: a short, whitespace free version of the name. You can either go with the suggestion (pressing enter), or give a new one. Here we go with "material_wu"
3. Short description of the extension: what data does it include, why should it be in EXIOBASE, any note important for understanding. This could be "Material data/domestic extraction for mining etc."
4. Data source: where does the data come from: "Vienna Economic University"
5. Main author compiling this extensions: you
6. Email address
7. Institute/Affiliation
8. Gitlab/github user name if available

After answering the questions, a directory /material_wu (or whatever was given in question 2: project_slug) will be established.

In there we find directories for data, src (code, scripts) and logs as well as an conda environment.yml file and a README.

The README was populated with the answers you gave initially. If you want to change anything you can just replace them here at any time.

In addition it contains detailed information on the project directory structure and how to establish the conda environment. Points which helps you to get started in there are marked with **NOTE** and can/should be deleted once you have setup the project.

## Version control

To track all changes we are using git. This should be done before we do any other changes to the extension data structure.

~~~ bash
git init
git add .
git commit -m "Project init for material wu data"
~~~

The project already includes a .gitignore, so only src files and such will be tracked.

We can see the first commit with

~~~ bash
git log
~~~

## Coding environment

Next we setup the Conda coding enviroment. This can be done with (again, replace conda with mamba if you have installed that before to speed-up things):


~~~ bash
mamba env create -f environment.yml
~~~

and activate with

~~~ bash
conda activate exio_sat_pipeline
~~~

Optional: If we want to use the ex2mag package (and we will do in this tutorial), we can install it into the activated environment with 

~~~ bash
pip install git+https://gitlab.com/exiobase/satellites/ex2mag@master
~~~

TODO: - with release of ex2mag put into the environment file

## Getting the raw data 

In this example we use the new materials data from WU. This data was sent as a compressed (zip) collection of xlsx files.

TODO: Add where to get the zip to follow the tutorial

As this file was sent manually (the data was not automatically received through some API or such), we copy the zip file to /raw_data_store  

Everything in /raw_data_store is also tracked by git and serves as an archive for all manually received data.

This was the last manual step, the rest of the pipeline will be solved programmatically.

## Start editing the data processing script

All processing scripts should be saved in the /src directory.
The template already includes some scripts which should serve as a starting point.

**Important**: The scripts already present as well as the functionality (logging and the function "run" in main.py), must stay in place in order to connect this extension subpipeline to the full EXIOBASE data pipeline later. 

So we open the main.py in there and add the first data processing step.

**Note**: Open the main.py and other files mentioned below to follow along.

Since the data comes packaged in a zip file, we first add the functionality to extract the data. This is just a small function, we can add to the main.py. 
So we add a 

~~~ python
import zipfile
~~~

at the beginning of the script. 

Then we define the data we are going to process, putting

~~~ python
SRC_DATA = conf.proj.raw_data_store / 'EXIOBASE_material extensions_1995-2019_final_20220222.zip'
~~~

after the import section.

The conf.proj and conf.data hold pathlib variables for each subfolder (proj for the project root, and conf.data for the data directory).

We then add our first data processing step:

~~~ python
def extract(archive, destination):
    logger.info(f"Extracting {archive} to {destination}")
    with zipfile.ZipFile(archive, 'r') as zr:
        zr.extractall(destination)
~~~

and then add the step to the run pipeline (under def run()):

~~~ python
logger.info("Preparing raw data")
extract_dir = conf.data.mkdir('clean_native/extracted')
extract(archive=SRC_DATA, destination=extract_dir)
~~~


We can check the content of the extracted folder with
~~~ python
list(extract_dir.glob('*'))
~~~

So the data will be in the subfolder 'EXIOBASE_detail', confirming with

~~~ python
list((extract_dir / 'EXIOBASE_detail').glob('*.xlsx'))
~~~

Opening one of the files we see that the data is stored in a sheet called 'data'.

So lets give that folder a name and read the data into pandas

~~~ python
logger.info("Loading datasets")
extracted_xlsx_folder = (extract_dir / 'EXIOBASE_detail')
df = pd.concat([pd.read_excel(f, sheet_name = 'data') for f in extracted_xlsx_folder.glob('*.xlsx')])
~~~

Since we imported ex2mag (TODO CHECK IF VALID) we automatically get an extension-module to df called 'extension' (note that the short versions 'ext' and 'e' also work).
This implements the specific functionality for handling MRIO extension data.

For example, we can look at the estimated structure of the dataframe with  

~~~ python
df.extension.structure
# or 
df.ext.structure
# or
df.e.structure
~~~

We can check if all required fields are present with

~~~ python
df.ext.validate()
~~~

and it tells us that the column 'provenance' is missing.
This column is used for tracking changes to the original data. 
We can add this easily in pandas and then tell the module to guess the structure again.

~~~ python
logger.info("Add missing provenance column")
df['provenance'] = 'original'
df.ext.identify_col_structure()
df.ext.validate()
~~~

The column headers of this specific file are already included in the ex2mag reconginzed header list.
In case of another extension where this would not be the case, we can support the identification by giving our own list of header names.
TODO: How to: In such cases please inform us about the new column headers so we can automate the process further.

~~~ python
specifc_names = dict(
    stressor=["PhysicalTypeName"],
    region=["CountryCode"],
    sector=["ProductTypeCode"],
    time=["AccountingYear"],
    value=["amount"],
    unit=["UnitCode"],
    provenance=["provenance"],
)
df.ext.identify_col_structure(possible_names=specifc_names)
~~~

Next, we do some predefined cleaning steps. 
These might not be stricly necessary in all cases, but can help to prevent some hard to find errors.
So first we replace NaN in string columns with

~~~ python
df.ext.replace_nan_in_string_columns()
~~~

and then we remove leading and trailing whitespaces around strings:

~~~ python
df.ext.strip_whitespace()
~~~

Now we can have a first look at the data.
With 

~~~ python
df.ext.stats
~~~

we can get a top-level overview about the content of the df and spread of numbers.

More detailed stats are available for each column type, e.g.

~~~ python
df.ext.regions
df.ext.stressors
df.ext.sectors
~~~

These always aggregates all other accounts into the specific section and show some stats on the numbers.
This is also useful if you just want to extract a list of all available stressors, regions, etc. with 

~~~ python
df.ext.stressors.index.to_list()
~~~


It is also possible to search for a specific stressor, region, or whatever with for example:

~~~ python
df.ext.search('metal')
~~~

This searches for any occurence of 'metal' in any of the columns.
The search string excepts regular expressions, so you can use these for more flexibility:
So for example to search for all p13.something product codes in the dataset we can use:

~~~ python
df.ext.search('p13.*')
~~~


