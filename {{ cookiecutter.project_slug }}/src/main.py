"""
Main entry point for compiling the {{cookiecutter.extension_name}}.

NOTE TO {{cookiecutter.main_author }} - you can delete this note afterwards:

This is a main python module - a function run (see below) must exist in this
file to make it workable with the extension pipeline.

The module imports the conf module which setups a logger (based on the loguro package)
which logs to stdout + 2 files in /logs (one for errors and one with all logs).
Please use the logger (as shown below, for all printing to stdin and error handling).

"""

import conf

logger = conf.logger


def run():
    """Main function to run to compile the complete extension."""
    logger.info("Start building the {{cookiecutter.extension_name}} data")
    logger.debug("example debug info")
    logger.warning("example warning message")
    logger.error(
        "example error message - this are also logged in ../logs/errorlog_DATE.log file"
    )
    logger.trace(
        "NOTE: do not use trace level - debug is the lowest we use for the extension"
    )
    logger.warning(
        "All these are just example for the use, delete all these logger for getting started"
    )

    # Returning all local variable to the calling function.
    # This helps for debugging, not necessary for the production run
    return locals()


if __name__ == "__main__":
    try:
        locals().update(run())
    except Exception as e:
        raise
