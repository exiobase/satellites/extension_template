import re
import sys


MODULE_REGEX = r"^[_a-zA-Z][_a-zA-Z0-9]+$"

proj_slug = "{{ cookiecutter.project_slug }}"

if not re.match(MODULE_REGEX, proj_slug):
    print(
        "ERROR: {} is not a valid project module name! Only letters, numbers, space and _ are allowed.".format(
            proj_slug
        )
    )

    # exits with status 1 to indicate failure
    sys.exit(1)
